const http = require("http");
const fs = require("fs");
const path = require("path");
const uuid = require("uuid");
const { error } = require("console");
const host = "localhost";
const port = process.env.PORT || 8080;

//creating a server
const server = http.createServer((request, response) => {
  if (request.url === "/html") {
    response.setHeader("content-type", "text/html");
    fs.readFile(path.join(__dirname, "index.html"), "utf-8", (error, data) => {
      if (error) {
        response.writeHead(404, "error occured"); // gives status code,content

       return response.end("Getting Error");
      } else {
        response.writeHead(200, "text/html");

       return  response.end(data);
      }
    });
  } else if (request.url === "/json") {
    response.setHeader("content-type", "application/json");
    fs.readFile(path.join(__dirname, "index.json"), "utf-8", (error, data) => {
      if (error) {
        response.writeHead(404, "error occured");
       return  response.end("Getting Error");
      } else {
        response.writeHead(200, "application/json");
        return response.end(data);
      }
    });

  } else if (request.url === "/uuid") {
    const uuid4 = uuid.v4();
    response.setHeader("content-type", "application/json");
    response.write(JSON.stringify({ uuid: `${uuid4}` }));
    return response.end();

  } else if (request.url.includes("/status/")) {
    response.setHeader("content-type", "application/json");
    let statusarray = request.url.split("/");
    let statuscode = parseInt(statusarray[statusarray.length - 1]);
    if (typeof statuscode === "number") {
      return response.end(`${statuscode} ,${http.STATUS_CODES[statuscode]}`);
    }

  } else if (request.url.includes("/delay/")) {

    let delayarray = request.url.split("/");

    let statuscode = 200;
    let delaytime = Number(delayarray[delayarray.length - 1]);
    response.setHeader("content-type", "application/json");

    setTimeout(() => {
      response.write(`${statuscode},${http.STATUS_CODES[statuscode]}`);
      return response.end();
    }, delaytime * 1000);
  }
});

//sending the request through port no. & Ip address
server.listen(port, host, () => {
  console.log(` Server is listening on port number:${port}`);
});
